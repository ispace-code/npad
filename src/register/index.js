
//
const register = {
	"numberOfColumns": 50,
	/**最大任务数 **/
	"numberOfRows": 30,
	/**标题栏高度 see global.less !!!**/
	"taskHeight": 40,
	"name":"xxxx",
	"application":[
		{
			"id":"0000",
			"name":"0000",
			"icon":"battery",
		},
		{
			"id":"0001",
			"name":"0001",
			"icon":"setting",
		},
		{
			"id":"0002",
			"name":"电池管理",
			"icon":"battery2",
			"bar":true,
		},
		{
			"id":"0003",
			"name":"0003",
			"icon":"calculator",
			"page":"calculator",
			"bar":true,
		},
		{
			"id":"0004",
			"name":"0004",
			"icon":"safari",
			
		},
		{
			"id":"0005",
			"name":"0005",
			"icon":"voice",
		},
		{
			"id":"0006",
			"name":"0006",
			"icon":"chrome",
		},
		{
			"id":"0007",
			"name":"系统信息",
			"page":"system",
			"icon":"pad",
		},
		{
			"id":"0008",
			"name":"0008",
			"icon":"setting",
		},
		{
			"id":"0009",
			"name":"0009",
			"icon":"firefox",
		},
		{
			"id":"0010",
			"name":"日志查看",
			"page":"Terminal",
			"icon":"terminal",
			"bar":true,
		},
		{
			"id":"0011",
			"name":"0011",
			"icon":"help",
			"bar":true,
		},
		{
			"id":"0012",
			"name":"0012",
			"icon":"photo",
			"bar":true,
		},
	]
}

export default register