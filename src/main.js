import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import './style.less';
import './icon.less';

import animate from 'animate.css'
import SwiperClass, { /* swiper modules... */ } from 'swiper'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import Vconsole from 'vconsole'
import VueTouch from 'vue-touch'
// import swiper module styles
import 'swiper/css'
// more module style...
var vConsole = new Vconsole()
// use swiper modules
SwiperClass.use([/* swiper modules... */])
const app = createApp(App)
app.config.productionTip = false;



app.use(store)
app.use(router)
app.use(animate)
app.use(VueAwesomeSwiper)
app.use(VueTouch, {name: 'v-touch'})
app.use(vConsole)
app.mount('#app')

