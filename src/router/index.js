import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: "/",
    name: "desk",
    component: () => import("../views/PadContainer.vue"),
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.baseURI),
  routes
})

export default router
