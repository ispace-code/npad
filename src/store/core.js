import REGISTER from '../register/index.js'
import arrays from '../utils/arrays.js'
import helper from '../utils/helper.js'
import {reactive} from 'vue'
export default  {
	namespaced: true,
	state:{
		numberOfColumns: 7,
		numberOfRows: 3,
		deskOfPage:[],
		/**
		[
			{
				id:"22",
				gridData:{
					id:"111",
					rows:[
						{
							id:"",
							cells:[
								{
									
								}
							]
						}
					]
				}
			}
		],**/
		dockOfList: [],
		tasks:reactive([])
	},
	mutations:{
		APP_INIT:()=> {
			
		},
		CORE_INIT:(state)=> {
			var pageSize = state.numberOfColumns * state.numberOfRows;
			var pageDate = {};
			var gridData = {};
			var row = {};var cells = [];
			REGISTER.application.forEach((item,index) => {
				if(index % pageSize==0){
					console.log("index===>["+index+"]  pageSize=>["+pageSize+"]")
					pageDate = {};
					pageDate.id = index /pageSize + 1 + "";
					gridData = {}
					gridData.id = index /pageSize + 1 + "";
					pageDate.gridData = gridData;
					gridData.rows = [];
					state.deskOfPage.push(pageDate);
				}
				
				if(index % state.numberOfColumns == 0) {
					row = {};
					row.id = "";
					cells = [];
					row.cells = cells;
					gridData.rows.push(row);
				}
				var cell = {};
				cell.id = item.id;
				cell.name = item.name;
				cell.icon = item.icon;
				cell.bar = (item.bar == null) ? false:item.bar;
				cells.push(cell);
				
				if(cell.bar == true) {
					var dock = {}
					dock.id = cell.id;
					dock.icon = cell.icon;
					dock.name = cell.name;
					state.dockOfList.push(dock);
				}
			})
		},
		OPEN_APPLICATION:(state,id)=> {
			let i = arrays.findIndexById(state.tasks,id)
			if(i < 0){
				let object = REGISTER.application.filter(t => t.id == id)[0]
				//console.log('id ========> ' + id + "===>" + JSON.stringify(object))
				let task = reactive({})
				task.name = object.name
				task.id = object.id
				task.icon = object.icon
				task.page = helper.ObjectNotNull(object.page) ? object.page : ""
				state.tasks.push(task)
			}
		},
		CLOSE_APPLICATION:(state,id)=> {
			let i = arrays.findIndexById(state.tasks,id)
			if(i < state.tasks.length){
				arrays.deleteByIndex(state.tasks,i)
			}
		}
	},
	actions:{
		init(context){
			context.commit("CORE_INIT")
		},
		openApplication(context, id) {
			context.commit('OPEN_APPLICATION', id)
		},
		closeApplication(context,id){
			context.commit("CLOSE_APPLICATION",id)
		},
	}
}