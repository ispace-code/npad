var dsBridge = require("dsbridge")
export default {
	//方法名，web传递给原生的数据，原生返回的回调函数
	call(name, data) {
		return dsBridge.call(name, data)
	},
	//方法名，web传递给原生的数据，原生返回的回调函数
	callAysn(name, data, callback) {
		return dsBridge.call(name, data, callback)
	},
	//方法名，原生传递过来的数据
	register(tag, callback) {
		dsBridge.register(tag, callback)
	}
}
